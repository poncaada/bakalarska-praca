#include "FaceDetector.h"
#include <iostream>
#include <algorithm>


//----------------------------------------------------------------------------------------------------
FaceDetector::FaceDetector( const std::string & cascadeModelFile )
{
	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	//SETTING DETECTION CONSTANTS

	m_ResizeScale = .4;

	m_ScaleFactor = 1.1;
	m_MinNeighbors = 5;
	m_MinSize = cv::Size(30, 30);
	m_MaxSize = cv::Size();

	m_VisColor = cv::Scalar(255, 0, 0);


	m_PrevWasDetected = false;
	m_RoiPadding = 20;

	m_ConsecutiveMatchingCount = 0;
	m_MaxConsecutiveMatches = 10;

	// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
	// LOADING CASCADE

	m_FaceCascade.load(cascadeModelFile);

	if (m_FaceCascade.empty()){
		std::cout << "ERROR: Cascase couldn't be loaded." << std::endl;;
		m_IsLoaded = false;
	}
	else {
		std::cout << "Face Cascade loading... SUCCESS!" << std::endl;
		m_IsLoaded = true;
	}
}

//----------------------------------------------------------------------------------------------------
bool FaceDetector::FindInRoi(cv::Mat &gray)
{
	cv::Mat  roi;

	int x = std::max(m_PrevFace.x - m_RoiPadding, 0);
	int y = std::max(m_PrevFace.y - m_RoiPadding, 0);
	int width = std::min(m_PrevFace.width + m_RoiPadding + m_RoiPadding, gray.cols - x);
	int height = std::min(m_PrevFace.height + m_RoiPadding + m_RoiPadding, gray.rows - y);

	cv::Rect faceArea(x, y, width, height);

	roi = gray(faceArea);

	m_FaceCascade.detectMultiScale(roi, m_Faces, m_ScaleFactor, m_MinNeighbors, 0, m_MinSize, m_MaxSize);

	if (!m_Faces.empty()) {

		// Choose a face that is closest to the previously detected face
		size_t index = 0;
		int diff;
		int min_diff = INT16_MAX;

		for (size_t i = 0; i < m_Faces.size(); ++i) {
			diff = abs(m_Faces[0].x - m_PrevFace.x) + abs(m_Faces[0].y - m_PrevFace.y) + abs(m_Faces[0].width - m_PrevFace.width) + abs(m_Faces[0].height - m_PrevFace.height);
			if (diff < min_diff) {
				min_diff = diff;
				index = i;
			}
		}

		// Save detected face data for next call
		m_PrevWasDetected = true;
		m_PrevFace = cv::Rect(m_Faces[index].x + x, m_Faces[index].y + y, m_Faces[index].width, m_Faces[index].height);
		m_PrevFaceImg = gray( m_Faces[index] );
		return true;
	}
	return false;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool FaceDetector::FindWithMatching(cv::Mat& gray)
{
	cv::Mat compRes = cv::Mat::zeros(cv::Size(gray.cols - m_PrevFaceImg.cols + 1, gray.rows - m_PrevFaceImg.rows + 1), CV_32FC1);

	cv::TemplateMatchModes matchMode = cv::TM_SQDIFF;

	cv::matchTemplate(gray, m_PrevFaceImg, compRes, matchMode);
	cv::normalize(compRes, compRes, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

	double minVal; double maxVal; cv::Point minLoc; cv::Point maxLoc;
	cv::Point matchLoc;

	minMaxLoc(compRes, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat());

	if (matchMode == cv::TM_SQDIFF || matchMode == cv::TM_SQDIFF_NORMED) {
		matchLoc = minLoc;
	}
	else {
		matchLoc = maxLoc;
	}
	
	// VISUALIZING MATCH DETECTION
	//rectangle(gray, matchLoc, cv::Point(matchLoc.x + m_PrevFaceImg.cols, matchLoc.y + m_PrevFaceImg.rows), cv::Scalar::all(0), 2, 8, 0);
	//rectangle(compRes, matchLoc, cv::Point(matchLoc.x + m_PrevFaceImg.cols, matchLoc.y + m_PrevFaceImg.rows), cv::Scalar::all(0), 2, 8, 0);
	//cv::imshow("Gray", gray);
	//cv::imshow("Res", compRes);

	m_PrevWasDetected = true;
	m_PrevFace = cv::Rect(matchLoc, m_PrevFaceImg.size());

	return true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
bool FaceDetector::Detect(cv::Mat &frame)
{
	bool isFound = false;

	cv::Mat gray;

	cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY);
	cv::resize(gray, gray, cv::Size(gray.size[1] * m_ResizeScale, gray.size[0] * m_ResizeScale), 0, 0, cv::INTER_LINEAR_EXACT);
	cv::equalizeHist(gray, gray);

	m_Faces.clear();
	
if (m_PrevWasDetected) {
	// SEARCH FACE IN LAST DETECTED REGION (ROI)
	isFound = FindInRoi(gray);

	if (!isFound && ++m_ConsecutiveMatchingCount <= m_MaxConsecutiveMatches) {
		// TEMPLATE MATCHING
		isFound = FindWithMatching(gray);
	}
	else {
		m_ConsecutiveMatchingCount = 0;
	}
}
		
if(!isFound){
	// SEARCH WHOLE IMAGE
	m_FaceCascade.detectMultiScale(gray, m_Faces, m_ScaleFactor, m_MinNeighbors, 0, m_MinSize, m_MaxSize);

	if (m_Faces.empty()) {
		m_PrevWasDetected = false;
		return false;
	}
	else {
		m_PrevWasDetected = true;
		m_PrevFace = m_Faces[0];
	}
}

	m_PrevFaceImg = gray(m_PrevFace);

	return true;
}