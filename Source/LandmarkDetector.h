#pragma once

#include <opencv2/core.hpp>
#include <opencv2/face.hpp>

class LandmarkDetector
{
protected:
    std::vector< std::vector<cv::Point2f> >     m_Landmarks;
    cv::Ptr<cv::face::Facemark>                 m_FacemarkLbf;
    bool                                        m_PrevDetected;

public:
	                            LandmarkDetector            (const std::string& modelFile);
    void						Detect                      (const cv::Mat& frame, const cv::Rect& face);

    cv::Point2f                 GetOne                      (int index);
    std::vector<cv::Point2f>    GetAll                      ();

    void                        ShowOne                     (int index, cv::Mat& frame, const cv::Scalar color = cv::Scalar(0, 255, 255), const int thickness = 1, bool showNumbers = false);
    void                        ShowAll                     (cv::Mat& frame, const cv::Scalar color = cv::Scalar(0, 255, 255), const int thickness = 1, bool ShowNumbers = false );

    inline size_t               GetCnt() {
        return m_Landmarks[0].size();
    }
};

