#pragma once

#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include <opencv2/face.hpp>


class FaceDetector
{
protected:
	cv::CascadeClassifier		m_FaceCascade;
	bool						m_IsLoaded;

	double						m_ResizeScale;

	std::vector<cv::Rect>		m_Faces;

	double						m_ScaleFactor;
	int							m_MinNeighbors;
	cv::Size					m_MinSize;
	cv::Size					m_MaxSize;

	cv::Scalar					m_VisColor;

	cv::Rect					m_PrevFace;
	cv::Mat						m_PrevFaceImg;
	bool						m_PrevWasDetected;

	int							m_RoiPadding;

	int							m_ConsecutiveMatchingCount;
	int							m_MaxConsecutiveMatches;


	bool						FindInRoi				( cv::Mat& gray );
	bool						FindWithMatching		( cv::Mat& gray );

public:
								FaceDetector			(const std::string& cascadeModelFile );
	bool						Detect					( cv::Mat& frame );

	inline cv::Rect				GetFace					()
	{
		return cv::Rect(m_PrevFace.x / m_ResizeScale, m_PrevFace.y / m_ResizeScale, m_PrevFace.width / m_ResizeScale, m_PrevFace.height / m_ResizeScale);
	}

	inline void					Show					(cv::Mat& frame)
	{
		cv::rectangle(frame, GetFace(), m_VisColor);
	}
};

