#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

//--------------------------------------------------------------------------------------------------------
//---------------------------------------------- CONSTANTS -----------------------------------------------
//--------------------------------------------------------------------------------------------------------

const std::string   CamWndName = "Camera: ";
const cv::Size      CamWndSize = cv::Size(800, 600);
const cv::Point     CamWndPos  = cv::Point(160, 90 + 300);

const std::string   DrawWndName = "Draw";
const cv::Size      DrawWndSize = cv::Size(800, 600);
const cv::Point     DrawWndPos  = cv::Point(160 + 800, 90 + 300);

const std::string   KeyWndName = "Keyboard shortcuts:";
const cv::Size      KeyWndSize = cv::Size(800, 210);
const cv::Point     KeyWndPos  = cv::Point(160, 180 - 35);

const std::string   DrawSetWndName = "Draw Settings";
const cv::Size      DrawSetWndSize = cv::Size(400, 210);
const cv::Point     DrawSetWndPos  = cv::Point(160 + 800, 180 - 35);

const std::string   ColorSetWndName = "Color RGB";
const cv::Size      ColorSetWndSize = cv::Size(400, 210);
const cv::Point     ColorSetWndPos  = cv::Point(160 + 1200, 180 - 35);

const size_t        LandmarkCount = 68;
const char          MarkDelim = ';';

// FILES
const std::string   dataFname = ".\\data\\";

const std::string   cascadeFname  = dataFname + "haarcascade_frontalface_default.xml";
const std::string   landmarkFname = dataFname + "lbfmodel.yaml";

const std::string   markPositionsFname = dataFname + "landmark_positions.txt";
const std::string   camIndexFname      = dataFname + "camera_index.txt";

// DRAWING
const int           MaxLineWidth = 35;

// FACE DETECTION

//--------------------------------------------------------------------------------------------------------
//---------------------------------------------- STRUCTURES ----------------------------------------------
//--------------------------------------------------------------------------------------------------------

enum class STATE {
    RUNNING,
    PAUSED,
    STARTING,
    FINISHING
};

struct DrawData {
    int red;
    int green;
    int blue;
    int alpha;
    int width;
};

//--------------------------------------------------------------------------------------------------------
//---------------------------------------------- FUNCTIONS -----------------------------------------------
//--------------------------------------------------------------------------------------------------------

void overlayImage(  const cv::Mat&          background,
                    const cv::Mat&          overlay,
                    cv::Mat&                output,
                    cv::Point2i             location,
                    double                  scale = 1);