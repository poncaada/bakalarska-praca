#pragma once

#include <opencv2/core.hpp>
#include "Defs.h"

class DrawBoard
{
protected:

	const std::string							m_WinName;
	size_t										m_WinWidth;
	size_t										m_WinHeight;
	cv::Point									m_WinPosition;
	
	cv::Mat										m_Drawing;
	cv::Mat										m_Background;
	cv::Mat										m_Board;

	cv::Point									m_PrevMousePos;

	DrawData									m_DrawData;
	bool										m_IsDrawingChanged;

	std::vector<cv::Point>						m_LandmarkPositions;
	size_t										m_LandmarkCnt;

	void					InitSettings				();
	void					InitMarks					();
	void					InnerMouseCallback			(int event, int x, int y, int flags);
	friend void				FriendCallBack				(int event, int x, int y, int flags, void* params);

public:
							DrawBoard					(const std::string & windowName, const cv::Size windowSize = cv::Size(928, 748), const cv::Point widowPosition = cv::Point(320, 300));
	void					Show						();
	void					Init						();
	void					Reset						();

	inline bool						IsChanged			() { 
		return m_IsDrawingChanged; 
	}
	
	inline cv::Mat					GetDrawing			(){
		return m_Drawing;
	}

	inline std::vector<cv::Point>	GetMarkPositions	(){
		return m_LandmarkPositions;
	}

	inline size_t					GetMarkCnt			() {
		return m_LandmarkCnt;
	}
};

