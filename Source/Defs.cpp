#include "Defs.h"

void overlayImage(const cv::Mat&    background,
    const cv::Mat&                  overlay,
    cv::Mat&                        output,
    cv::Point2i                     location,
    double                          scale)
{
    cv::Mat foreground;

    background.copyTo(output);

    if (scale != 1)
        cv::resize(overlay, foreground, cv::Size(overlay.cols * scale, overlay.rows * scale), 0, 0, cv::INTER_LINEAR_EXACT);
    else
        overlay.copyTo(foreground);

    // start at the row indicated by location, or at row 0 if location.y is negative.
    for (int y = std::max(location.y, 0); y < background.rows; ++y)
    {
        int fY = y - location.y; // because of the translation

        // we are done of we have processed all rows of the foreground image.
        if (fY >= foreground.rows)
            break;

        // start at the column indicated by location, 

        // or at column 0 if location.x is negative.
        for (int x = std::max(location.x, 0); x < background.cols; ++x)
        {
            int fX = x - location.x; // because of the translation.

            // we are done with this row if the column is outside of the foreground image.
            if (fX >= foreground.cols)
                break;

            // determine the opacity of the foreground pixel, using its fourth (alpha) channel.
            int index = fY * foreground.step + fX * foreground.channels() + 3;
            double opacity = ((double)foreground.data[index]) / 255.0;


            // and now combine the background and foreground pixel, using the opacity, 

            // but only if opacity > 0.
            for (int c = 0; opacity > 0 && c < output.channels(); ++c)
            {
                unsigned char foregroundPx =
                    foreground.data[fY * foreground.step + fX * foreground.channels() + c];
                unsigned char backgroundPx =
                    background.data[y * background.step + x * background.channels() + c];
                output.data[y * output.step + output.channels() * x + c] =
                    backgroundPx * (1. - opacity) + foregroundPx * opacity;
            }
        }
    }

}



/*    int maxWidth  = 1920 - 200;
    int maxHeight = 1080 - 200;

    cv::Mat testLayout = cv::Mat(cv::Size(maxWidth, maxHeight), CV_8UC3, cv::Scalar(20, 20, 20));

    cv::rectangle(testLayout, cv::Point(0, 0),   cv::Point(100, maxHeight - 1), cv::Scalar(0, 0, 255));
    cv::rectangle(testLayout, cv::Point(100, 0), cv::Point((maxWidth - 100)/2 + 100, maxHeight - 1), cv::Scalar(0, 0, 255));
    cv::rectangle(testLayout, cv::Point((maxWidth - 100) / 2 + 100, 0), cv::Point(maxWidth - 1, maxHeight - 1), cv::Scalar(0, 0, 255));

    while (1) {
        cv::imshow("test", testLayout);
        cv::waitKey(30);
    }*/