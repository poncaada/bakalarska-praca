#include "LandmarkDetector.h"

#include <vector>
#include <opencv2/imgproc.hpp>


LandmarkDetector::LandmarkDetector(const std::string& modelFile)
{
    m_PrevDetected = false;

    m_FacemarkLbf = cv::face::FacemarkLBF::create();
    m_FacemarkLbf->loadModel(modelFile);
}


void LandmarkDetector::Detect(const cv::Mat& frame, const cv::Rect& face)
{
    std::vector<cv::Rect> faces;
    faces.emplace_back(face);

    m_PrevDetected = m_FacemarkLbf->fit(frame, faces, m_Landmarks);
}

cv::Point2f LandmarkDetector::GetOne(int index)
{
    if( index > m_Landmarks.size() )
        return m_Landmarks[0][index];
    return cv::Point2f();
}

std::vector<cv::Point2f> LandmarkDetector::GetAll()
{
    return m_Landmarks[0];
}

void LandmarkDetector::ShowOne(int index, cv::Mat& frame, const cv::Scalar color, const int thickness, bool showNumbers)
{
    if (m_PrevDetected && index < m_Landmarks[0].size()) {
        cv::circle(frame, m_Landmarks[0][index], thickness, color, cv::FILLED);
        if (showNumbers)
            cv::putText(frame, std::to_string(index), m_Landmarks[0][index], cv::FONT_HERSHEY_PLAIN, .5, color);
    }
}

void LandmarkDetector::ShowAll(cv::Mat& frame, const cv::Scalar color, const int thickness, bool showNumbers)
{
    if (m_PrevDetected)
        for (unsigned long i = 0; i < m_Landmarks[0].size(); i++) {
            cv::circle(frame, m_Landmarks[0][i], thickness, color, cv::FILLED);
            if( showNumbers )
                cv::putText(frame, std::to_string(i), m_Landmarks[0][i], cv::FONT_HERSHEY_PLAIN, .5, color);
        }
}
