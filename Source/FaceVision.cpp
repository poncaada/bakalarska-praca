// FaceVision.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <iostream>
#include <fstream>
#include "Defs.h"
#include "DrawBoard.h"
#include "FaceDetector.h"
#include "LandmarkDetector.h"

#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>


void createKeyWindow()
{
    cv::namedWindow(KeyWndName, cv::WINDOW_NORMAL);
    cv::moveWindow(KeyWndName, KeyWndPos.x, KeyWndPos.y);
    cv::resizeWindow(KeyWndName, KeyWndSize);
    cv::Mat keys(KeyWndSize, CV_8UC3, cv::Scalar(150, 150, 150));
    cv::putText(keys, "ESC -> Exit application", cv::Point(50, 40), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 125), 2);
    cv::putText(keys, " 0  -> Reset drawing", cv::Point(50, 80), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0), 2);
    cv::putText(keys, " 1  -> Show face rectangle", cv::Point(50, 110), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0), 2);
    cv::putText(keys, " 2  -> Show facial features", cv::Point(50, 140), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0), 2);
    cv::putText(keys, " 3  -> Flip camera", cv::Point(400, 80), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0), 2);
    cv::putText(keys, " 4  -> Toggle webcam video", cv::Point(400, 110), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0), 2);
    cv::putText(keys, " 5  -> Toggle mapping", cv::Point(400, 140), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 0), 2);
    cv::imshow(KeyWndName, keys);
}


int main()
{
    //--------------------------------------------------------------------------------------------------------
    // SETUP
    //--------------------------------------------------------------------------------------------------------
    size_t                          CAM_INDEX = 0;
    STATE                           appState = STATE::STARTING;

    DrawBoard                       drawBoard(DrawWndName, DrawWndSize, DrawWndPos);

    // Camera
    cv::VideoCapture                camDevice;
    cv::Mat                         camOut, 
                                    frame;
    std::string                     camWindowName;

    // Detection
    bool                            isFaceDetected;
    FaceDetector                    faceDetector(cascadeFname);
    LandmarkDetector                landmarkDetector(landmarkFname);
    cv::Rect                        face;

    // Draw
    cv::Mat                         drawing;
    cv::Mat                         warped;

    // Settings
    bool                            isDetectRect    = false;
    bool                            isDetectMark    = false;
    bool                            isFlipped       = false;
    bool                            isMappingOn     = true;

    // FPS
    //double                          fpsMax = 0;
    double                          t, tt, fps;

    int                             key;

    //--------------------------------------------------------------------------------------------------------
    // INIT
    //--------------------------------------------------------------------------------------------------------

    // Load camera
    std::ifstream camIndexFile(camIndexFname, std::ios_base::in);
    if (camIndexFile.is_open()) {
        camIndexFile >> CAM_INDEX;
    }
    else {
        std::cout << "Warning: Unable to load file \"camera_index.txt\". Using default index (0).\n";
    }

    camDevice.open(CAM_INDEX);
    if (camDevice.isOpened() == false)
    {
        std::cout << "Error: Webcam connect unsuccessful.\n";
        return(0);
    }

    camWindowName = CamWndName + camDevice.getBackendName();
    camDevice.set(cv::CAP_PROP_FPS, 30);
    camDevice.set(cv::CAP_PROP_FRAME_WIDTH, CamWndSize.width);
    camDevice.set(cv::CAP_PROP_FRAME_HEIGHT, CamWndSize.height);

    camDevice >> camOut;

    cv::namedWindow(camWindowName, cv::WINDOW_NORMAL);
    cv::moveWindow(camWindowName, CamWndPos.x, CamWndPos.y);
    cv::resizeWindow(camWindowName, CamWndSize);

    drawBoard.Init();

    createKeyWindow();

    //--------------------------------------------------------------------------------------------------------
    // RUN CYCLE
    //--------------------------------------------------------------------------------------------------------

    appState = STATE::RUNNING;

    while( true )
    {
        t = cv::getTickCount();
        tt = 0;
        fps = 0;

        //--------------------------------------------------------------------------------------------------------
        // Drawing
        if (drawBoard.IsChanged()) {
            drawBoard.Show();
            drawBoard.GetDrawing().copyTo(drawing);
            cv::resize(drawing, drawing, CamWndSize);
        }

        //--------------------------------------------------------------------------------------------------------
        // Face Detection
        if (appState != STATE::PAUSED) {
            camDevice >> camOut;
        }

        camOut.copyTo(frame);

        if (isFlipped)
            cv::flip(frame, frame, 1);

        if (appState != STATE::PAUSED) {
            isFaceDetected = faceDetector.Detect(frame);

            if (isFaceDetected) {

                face = faceDetector.GetFace();

                landmarkDetector.Detect(frame, face);
            }
        }


        if ( isFaceDetected && drawBoard.GetMarkCnt() == landmarkDetector.GetCnt() ) {
            if (isDetectRect)   faceDetector.Show(frame);
            if (isDetectMark)   landmarkDetector.ShowAll(frame);
            if (isMappingOn) {
                cv::Mat homography = cv::findHomography(drawBoard.GetMarkPositions(), landmarkDetector.GetAll());
                cv::warpPerspective(drawing, warped, homography, drawing.size());
                overlayImage(frame, warped, frame, cv::Point(0, 0));
            }
        }


        // UPDATE FPS
        tt = ((double)cv::getTickCount() - t) / cv::getTickFrequency();
        fps = 1 / tt;
       // if (fps> fpsMax)
            //fpsMax = fps;

        putText(frame, cv::format("FPS = %.2f", fps), cv::Point(10, 50), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 2);
        //putText(frame, cv::format("MAX FPS = %.2f", fpsMax), cv::Point(10, 100), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 2);
        
        
        //--------------------------------------------------------------------------------------------------------
        //Plot Final Image
        cv::imshow(camWindowName, frame);

        key = cv::waitKey(30);

        if (key == 27)
            break;
        switch (key)
        {
        case '0':
            drawBoard.Reset();
            break;
        case '1':
            isDetectRect = !isDetectRect;
            break;
        case '2':
            isDetectMark = !isDetectMark;
            break;
        case '3':
            isFlipped = !isFlipped;
            break;
        case '4':
            appState = appState == STATE::PAUSED ? STATE::RUNNING : STATE::PAUSED;
            break;
        case '5':
            isMappingOn = !isMappingOn;
        case -1:
            break;
        default:
            break;
        }            
    }

    camDevice.release();

    return 0;
}
