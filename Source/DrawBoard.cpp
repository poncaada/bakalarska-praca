#include "DrawBoard.h"

#include <opencv2/highgui.hpp>
#include <fstream>
#include <iostream>


static void TB_UpdateColorPreview(int value, void* params)
{
    DrawData* cd = (DrawData*)params;
    cv::imshow(ColorSetWndName, cv::Mat(cv::Size(50, 50), CV_8UC4, cv::Scalar(cd->blue, cd->green, cd->red, cd->alpha)));
}

DrawBoard::DrawBoard(const std::string& windowName, const cv::Size windowSize, const cv::Point windowPosition)
    : m_WinName(windowName), m_WinWidth(windowSize.width), m_WinHeight(windowSize.height), m_WinPosition(windowPosition)
{
    m_Background = cv::Mat(cv::Size(m_WinWidth, m_WinHeight), CV_8UC3, cv::Scalar(200, 200, 200));
    m_DrawData = DrawData{0, 0, 0, 255, 4};

    Reset();
}

void FriendCallBack(int event, int x, int y, int flags, void* obj)
{
    DrawBoard* drawBord = static_cast<DrawBoard*>(obj);
    if (drawBord)
        drawBord->InnerMouseCallback(event, x, y, flags);
}

void DrawBoard::InitSettings()
{
    cv::namedWindow(ColorSetWndName, cv::WINDOW_NORMAL);
    cv::resizeWindow(ColorSetWndName, ColorSetWndSize);
    cv::moveWindow(ColorSetWndName, ColorSetWndPos.x, ColorSetWndPos.y);
    cv::createTrackbar("Red", ColorSetWndName, &m_DrawData.red, 255, TB_UpdateColorPreview, (void*)&m_DrawData);
    cv::createTrackbar("Green", ColorSetWndName, &m_DrawData.green, 255, TB_UpdateColorPreview, (void*)&m_DrawData);
    cv::createTrackbar("Blue", ColorSetWndName, &m_DrawData.blue, 255, TB_UpdateColorPreview, (void*)&m_DrawData);

    cv::namedWindow(DrawSetWndName, cv::WINDOW_NORMAL);
    cv::resizeWindow(DrawSetWndName, DrawSetWndSize);
    cv::moveWindow(DrawSetWndName, DrawSetWndPos.x, DrawSetWndPos.y);
    cv::createTrackbar("Alpha", DrawSetWndName, &m_DrawData.alpha, 255, TB_UpdateColorPreview, (void*)&m_DrawData);
    cv::createTrackbar("Line Width", DrawSetWndName, &m_DrawData.width, MaxLineWidth, TB_UpdateColorPreview, (void*)&m_DrawData);

    TB_UpdateColorPreview(0, (void*)&m_DrawData);
}

void DrawBoard::InitMarks()
{
    // DRAW MARKS
    std::ifstream myfile(markPositionsFname);
    std::string positions;
    size_t delimPos;
    int xPos, yPos,
        xMax, yMax,
        xDif, yDif,
        xOff, yOff,
        widNew, heiNew;

    // get marks max positions
    std::getline(myfile, positions);
    delimPos = positions.find(MarkDelim);

    xMax = std::stoi(positions.substr(2, delimPos));
    yMax = std::stoi(positions.substr(delimPos + 3));

    xDif = (int)m_WinWidth - xMax;
    yDif = (int)m_WinHeight - yMax;

    if (xDif > yDif) {
        xOff = std::abs(m_WinHeight * xMax / (double)yMax - m_WinWidth) / 2 + 100;
        yOff = 50;
        widNew = m_WinHeight * xMax / yMax - 200;
        heiNew = m_WinHeight - 100;
    }
    else {
        xOff = 50;
        yOff = std::abs(m_WinWidth * yMax / (double)xMax - m_WinHeight) / 2 + 100;
        widNew = m_WinWidth - 100;
        heiNew = m_WinWidth * yMax / xMax - 200;
    }

    for (size_t i = 0; i < LandmarkCount; ++i) {

        // load marks
        std::getline(myfile, positions);
        delimPos = positions.find(MarkDelim);

        xPos = std::stoi(positions.substr(0, delimPos));
        yPos = std::stoi(positions.substr(delimPos + 1));

        m_LandmarkPositions.emplace_back(cv::Point(xPos * widNew / xMax + xOff, yPos * heiNew / yMax + yOff));

        cv::circle(m_Background, m_LandmarkPositions[i], 1, cv::Scalar(127, 127, 0), 3);
        //cv::putText(m_Background, std::to_string(i + 1), m_LandmarkPositions[i], cv::FONT_HERSHEY_SIMPLEX, 0.3, cv::Scalar(0, 0, 255));
    }
}

void DrawBoard::Init()
{
    InitSettings();

    cv::namedWindow(m_WinName, cv::WINDOW_NORMAL);
    cv::resizeWindow(m_WinName, cv::Size(m_WinWidth, m_WinHeight));
    cv::moveWindow(m_WinName, m_WinPosition.x, m_WinPosition.y);
    cv::setMouseCallback(m_WinName, FriendCallBack, this);

    InitMarks();

    m_LandmarkCnt = m_LandmarkPositions.size();
}

void DrawBoard::Reset()
{
    m_Drawing = cv::Mat::zeros(cv::Size(m_WinWidth, m_WinHeight), CV_8UC4);
    m_IsDrawingChanged = true;
}


void DrawBoard::Show()
{
    if (m_IsDrawingChanged) {
        overlayImage(m_Background, m_Drawing, m_Board, cv::Point(0, 0));
        m_IsDrawingChanged = false;
    }
    cv::imshow(m_WinName, m_Board);
}

void DrawBoard::InnerMouseCallback(int event, int x, int y, int flags)
{

    switch (event)
    {
    case cv::EVENT_LBUTTONDOWN:
        //cv::circle(m_Board, cv::Point(x, y), 1, m_DrawColor, 5);
        m_PrevMousePos = cv::Point(x, y);
        m_IsDrawingChanged = true;
        break;
    case cv::EVENT_LBUTTONUP:
        cv::line(m_Drawing, m_PrevMousePos, cv::Point(x, y), cv::Scalar(m_DrawData.blue, m_DrawData.green, m_DrawData.red, m_DrawData.alpha), m_DrawData.width + 1);
        m_IsDrawingChanged = true;
        break;
    case cv::EVENT_RBUTTONDOWN:
        break;
    case cv::EVENT_RBUTTONUP:
        break;
    case cv::EVENT_MBUTTONDOWN:
        break;
    case cv::EVENT_MBUTTONUP:
        break;
    case cv::EVENT_MOUSEMOVE:
        if (flags & cv::EVENT_FLAG_LBUTTON) {
            cv::line(m_Drawing, m_PrevMousePos, cv::Point(x, y), cv::Scalar(m_DrawData.blue, m_DrawData.green, m_DrawData.red, m_DrawData.alpha), m_DrawData.width + 1);
            m_PrevMousePos = cv::Point(x, y);
            m_IsDrawingChanged = true;
        }
        break;
    }
}
