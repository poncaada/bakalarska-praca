# Mapování rastrové grafiky na lidskou tvář ve smíšené realitě

### Popis:
Táto práca sa zaoberá problematikou vytvárania jednoduchých tvárových filtrov za pomoci knižnice OpenCV. Cieľom je vytvoriť prototyp aplikácie schopnej tvorby a mapovania rastrovej grafiky na orientačné body tváre v zmiešanej realite. Na detekciu tvárí bola použitá *Haar-like casscade* detekcia a na tvárové črty algoritmus regresných lokálnych binárnych príznakov. Výsledkom je funkčná počítačová aplikácia pre systém Windows z jednoduchým užívateľským rozhraním. Umožňuje užívateľovi intuitívnu tvorbu jednoduchej grafiky, ktorú aplikácia následne pretransformuje a mapuje na črty tváre detekovanej vo vstupnom videu webkamery.

### Predpoklady:
Program je vytvorený pre operačný systém _Windows 10_ a kompatibilita s inými operačnými systémami nie je zaručená. Rozmery užívateľského prostredia programu sú prispôsobné na FullHD monitor (1920x1080).

### Spustenie:
Pre spustenie stiahnite súbor **Facevision_x64Release.zip** a rozbaľte v ľubovoľnom priečinku. Následne 2x kliknite ľavým tlačidlom myši na súbor **FaceVision.exe** nachádzajúci sa v rozbalenom priečinku. 

### Nastavenie:
Ak sa program po načítaní ihneď ukončí, je možné, že je nastavený nesprávny index kamery. Zvoľte následujúci postup aj v prípade, že chcete zmeniť kameru, s ktorou bude program pracovať. Pre zmenu indexu kamery otvorte súbor **data\camera_index.txt**, zmente číslo vo vnútri súboru na požadovaný index a súbor následne uložte.